// tạo mảng lưu object
var DSSV = [];
// get JSON khi load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");

if (dataJson != null) {
  // convert JSON=>array
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new Sinhvien(
      item.ma,
      item.ten,
      item.mail,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    DSSV.push(sv);
  }
}
renderDSSV(DSSV);

function themSinhVien() {
  var sv = layThongTinTuForm();
  DSSV.push(sv);
  // convert array =>JSON
  var dataJson = JSON.stringify(DSSV);
  // save JSON
  localStorage.setItem("DSSV_LOCAL", dataJson);
  renderDSSV(DSSV);
}

function xoaSV(id) {
  // tìm vị trí
  var viTri = -1;
  for (var i = 0; i < DSSV.length; i++) {
    var sv = DSSV[i];
    if (sv.ma == id) {
      viTri = i;
      break;
    }
  }
  DSSV.splice(viTri, 1);
  renderDSSV(DSSV);
}
function suaSV(id) {
  // cach 1
  // var viTri = -1;
  // for (var i = 0; i < DSSV.length; i++) {
  //   var sv = DSSV[i];
  //   if (sv.ma == id) {
  //     viTri = i;
  //     break;
  //   }
  // }
  // if (viTri != -1) {
  //   console.log(viTri);
  // }

  // cach 2
  var viTri = DSSV.findIndex(function (item) {
    return item.ma == id;
  });
  if (viTri != -1) {
    showThongTin(DSSV[viTri]);
  }
  // chan user sua id
  document.getElementById("txtMaSV").disabled = true;
}

// cap nhat sv
function capNhatSinhVien() {
  var sv = layThongTinTuForm();
  var viTri = DSSV.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  if (viTri != -1) {
    DSSV[viTri] = sv;
  }
  renderDSSV(DSSV);

  resetForm();
}

// reset form
function resetForm() {
  document.getElementById("formQLSV").reset();
}
